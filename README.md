# Msc. Thesis Project

This project is to replicate and/or try the results produced for the Master Thesis titled 'Changing trends in female authorship of Software Engineering research community'.

## Installation

The project uses Python version 3.10. 
Install the required libraries below using the package manager [pip](https://pip.pypa.io/en/stable/).

```bash
pip install gender-guesser
pip install matplotlib
pip install pandas
pip install scikit-learn
pip install numpy
pip install scipy
pip install wordcloud

```

## Usage

The code in the project is divided into 4 classes for all 4 research questions in the Thesis.
