# import gender_guesser

import pandas as pd
import matplotlib.pyplot as plt
import gender_guesser.detector as gender

file_path = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_original.csv"
file_path_save = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_original.csv"
file_path_female = '/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_female.csv'

"""
Preprocess data for RQ1
"""


# Split authors for each publication
def prepare_csv():
    # Read csv in a data_frame
    data_frame = pd.read_csv(file_path, encoding='utf-8')

    # Split names with ';' and convert single row into multiple rows with names. Rest of the rows are duplicated.
    data_frame['Author full names'] = data_frame['Author full names'].str.split(";")
    data_frame = data_frame.explode('Author full names')

    # Get First name of each Author and add new column first_names in data_frame
    # data_frame['first_name'] = data_frame['Author full names'].str.extract(' ( [a-zA-Z- ]+ ) ', expand=False).str.strip()
    data_frame['first_name'] = data_frame['Author full names'].str.split(',').str.get(-1)
    data_frame['first_name'] = data_frame['first_name'].str.split('(').str.get(0)
    data_frame['first_name'] = data_frame['first_name'].str.strip()
    print(data_frame['first_name'])

    # Save CSV to disk
    data_frame.to_csv(file_path_save, mode='w+')

    return data_frame['first_name']


# Genderize names in the column 'first_name'
def gender_guesser():
    names_list = prepare_csv().values.tolist()
    # print(names_list)

    gender_array = []

    # Gender guesser
    detector = gender.Detector()
    for name in names_list:
        # print(detector.get_gender(u"%s" % name))
        if detector.get_gender(u"%s" % name) == 'mostly_female':
            gender_array.append('female')
        elif detector.get_gender(u"%s" % name) == 'mostly_male':
            gender_array.append('male')
        else:
            gender_array.append(detector.get_gender(u"%s" % name))

    data_frame = pd.read_csv(file_path_save)
    data_frame['Gender'] = gender_array
    data_frame.to_csv(file_path_save, mode='w+')


# Remove names labeled 'unknown' and 'andy'
def clean_file():
    data_frame = pd.read_csv(file_path)
    values = ["unknown", "andy"]
    data_frame = data_frame[data_frame.Gender.isin(values) == False]
    # data_frame.drop(["unknown", "andy"], inplace=True)

    # Save CSV to disk
    data_frame.to_csv(file_path, mode='w+')


# Filter rows in dataset, where Gender is 'female'
def filter_female_authors():
    data_frame = pd.read_csv(file_path)
    values = ["male"]
    data_frame = data_frame[data_frame.Gender.isin(values) == False]
    # data_frame.sort_values(by='Cited by', ascending=False, inplace=True)

    data_frame.to_csv(file_path_female, mode='w+')


"""
Plot graphs for RQ1
"""


# Plot graphs for 'Gender' distributed along 'Year'
def plot_graph_01():
    data_frame = pd.read_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_original.csv',
                             dtype={"Year": "string", "Gender": "string"})
    pd.crosstab(data_frame['Year'], data_frame['Gender']).plot.bar(color=['#00D1D1'], width=0.8)  # '#007575'
    plt.show()


# Combined bar graph to show participation of both genders in each Conference and Journal
def plot_conference_journal():
    publications = [{"name": "ICSE", "female": 20, "male": 80},
                    {"name": "ASE", "female": 16, "male": 84},
                    {"name": "FSE", "female": 23, "male": 77},
                    {"name": "RE", "female": 30, "male": 70},
                    {"name": "EASE", "female": 25, "male": 75},
                    {"name": "ESEM", "female": 17, "male": 83},
                    {"name": "TSE", "female": 17, "male": 83},
                    {"name": "TOSEM", "female": 17, "male": 83},
                    {"name": "EMSE", "female": 20, "male": 80},
                    {"name": "JSS", "female": 19, "male": 81},
                    {"name": "IST", "female": 22, "male": 78}]
    # Create a pandas dataframe from the data
    df = pd.DataFrame(publications)
    ax = df.plot(x="name", y=["female", "male"], kind="barh", rot=0, color=['#00D1D1', '#007575'], width=0.8)
    # for container in ax.containers:
    ax.bar_label(ax.containers[0], labels=[f'{p}%' for p in df['female']], fontsize=8, color="gray")
    ax.bar_label(ax.containers[1], labels=[f'{p}%' for p in df['male']], fontsize=8, color="gray")
    ax.legend(bbox_to_anchor=(0.4, 1.0))
    ax.set_xlabel("Percentages")
    ax.set_ylabel("Conferences/ Journals")
    # Plot grouped horizontal bar chart
    plt.show()


"""
Machine learning method for the genderization of Author names
"""


# ML Packages
# from sklearn.feature_extraction.text import CountVectorizer
# from sklearn.feature_extraction.text import TfidfVectorizer
# from sklearn.feature_extraction import DictVectorizer
# from sklearn.model_selection import train_test_split
# from sklearn.naive_bayes import MultinomialNB

# def guess_gender_machine_learning():
#     # Load our data
#     df = pd.read_csv('/Users/ummerabab-/Downloads/names_dataset.csv')
#     # test_df = pd.read_csv('/Users/ummerabab-/Downloads/name_gender.csv')
#     print(df.head())
#
#     # Checking for Missing Values
#     df.isnull().isnull().sum()
#
#     # Number of Female Names
#     df[df.sex == 'F'].size
#
#     df_names = df
#
#     # Replacing All F and M with 0 and 1 respectively
#     # df_names.sex.replace({'F': 0, 'M': 1}, inplace=True)
#     # test_df.sex.replace({'F': 0, 'M': 1}, inplace=True)
#     df_names.sex.unique()
#
#     X_features = df['name']
#     # X_test_features = df['name']
#
#     # Feature Extraction
#     cv = CountVectorizer()
#     X = cv.fit_transform(X_features)
#     cv.get_feature_names_out()
#
#     # X_test = cv.fit_transform(X_test_features)
#     # cv.get_feature_names_out()
#
#     # In supervised machine learning we have features and labels.
#     # The features are the descriptive attributes.
#     # Label is what we're attempting to predict.
#     # Features
#     X
#     # Labels
#     Y = df['sex']
#     # Y_test = test_df['sex']
#     X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=42)
#
#     # Naive Bayes Classifier
#     clf = MultinomialNB()
#     clf.fit(X_train, Y_train)
#     clf.score(X_test, Y_test)
#
#     # Accuracy of our Model
#     print("Accuracy of Model", clf.score(X_test, Y_test) * 100, "%")
#     print("Accuracy of Model", clf.score(X_train, Y_train) * 100, "%")
#
#     # Sample Prediction
#     names_list = prepare_csv().values.astype('U').tolist()
#     vector = cv.transform(names_list).toarray()
#     # sample_name = ['Christoph', 'Tobias', 'Paul', 'Michael', 'Manal', 'Ana', 'Austin', 'Julian', 'Benjamin', 'Aiden', 'Dylan', 'James', 'Robert', 'Ipek', 'Chris', 'Christopher', 'Marouane', 'Manish', 'Chetan', 'Suman', 'Sean', 'Henry', 'Ozgur', 'Siamak', 'Irina', 'Huibin Mary', 'Christina', 'Chewy', 'Shivam', 'Paul Luo', 'Makayla', 'Denae', 'Paige', 'Der']
#     # vector = cv.transform(sample_name).toarray()
#     # Female is 0, Male is 1
#     # print(clf.predict(vector))
#
#     df_from_disk = pd.read_csv("/Users/ummerabab-/Downloads/icse_scopus.csv")
#     df_from_disk['ML'] = clf.predict(vector)
#     df_from_disk.to_csv('/Users/ummerabab-/Downloads/icse_scopus.csv', mode='w+')
