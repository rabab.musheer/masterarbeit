import matplotlib.pyplot as plt
import pandas as pd


# Distribution of all labels 'unknown', 'andy', 'male', 'female' in the dataset
def gender_dist():
    data_frame = pd.read_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_original.csv',
                             dtype={"Year": "string", "Gender": "string"})

    # Number of male authors
    male_count = data_frame['Gender'].value_counts()['male']
    print(male_count)
    # Number of female authors
    female_count = data_frame['Gender'].value_counts()['female']
    print(female_count)
    # Number of unknown authors
    unknown_count = data_frame['Gender'].value_counts()['unknown']
    print(unknown_count)
    # Number of andy authors
    andy_count = data_frame['Gender'].value_counts()['andy']
    print(andy_count)
    # Total number of authors
    # Drop duplicate names to count total number of names
    # data_frame.drop_duplicates(subset=['Author full names'], inplace=True)
    count_row = data_frame.shape[0]
    print(count_row)


# Distribution of data
def plot_pie_chart():
    # Create a pie chart
    labels = ['Male', 'Female', 'Unknown', 'Andy']
    data = [33555, 8713, 29780, 5276]
    plt.pie(data, labels=labels, autopct='%1.0f%%', colors=['#007575', '#00D1D1', '#F0EAE4', '#5e9494'])
    # plt.title('Distribution of values by type')
    plt.show()
