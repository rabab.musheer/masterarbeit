import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from wordcloud import WordCloud

file_path = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_female.csv"
# 2000 - 2005
file_path_01 = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/combined_female_authors_01.csv"
# 2006 - 2010
file_path_02 = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/combined_female_authors_02.csv"
# 2011 - 2015
file_path_03 = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/combined_female_authors_03.csv"
# 2016 - 2020
file_path_04 = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/combined_female_authors_04.csv"
# 2021 - 2022
file_path_05 = "/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/combined_female_authors_05.csv"


# Method to split csv into 5 years i.e. 2000 - 2005; 2005 - 2010; 2010 - 2015; 2015 - 2020; 2020 - 2022
def split_csv():
    data_frame = pd.read_csv(file_path)
    values = [2021, 2022]
    data_frame = data_frame[data_frame['Year'].isin(values) == True]

    data_frame.to_csv(file_path_05, mode='w+')


# Create word cloud for each year bracket.
def create_wordcloud():
    data_frame = pd.read_csv(file_path)

    # Generate a string from keywords in 'Author Keywords'
    # if data_frame['Author Keywords'].isnull().values.any():
    #     text = " ".join(str(keyword) for keyword in data_frame['Author Keywords']).lower()
    #     text = text.replace("; ", " ")
    #     # text = text.replace("  ", "")
    #     text = text.replace("nan", "")
    #     text = text.replace("software", "")
    #     text = text.replace("software engineering", "")
    #     # print(text)
    #     # print("There are {} words in the combination of all review.".format(len(text)))

    # Define a list of strings representing the author keywords
    keyword_array = []
    for value in data_frame['Author Keywords']:
        if str(value) != 'nan':
            keyword = str(value).lower().split('; ')
            keyword_array.extend(keyword)
    print(len(keyword_array))

    # Create a frequency distribution of the n-grams in the array
    freq_dict = {}
    for ngram in keyword_array:
        if ngram != 'software engineering':
            freq_dict[ngram] = freq_dict.get(ngram, 0) + 1
    # print(freq_dict)

    df = pd.DataFrame(list(freq_dict.items()), columns=["Word", "Frequency"])
    df = df.sort_values(by='Frequency', ascending=False)
    # df.to_csv("/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/word_freq_combined.csv", mode='w+')
    # print(df)

    # Generate the word cloud
    # stopwords = set(STOPWORDS)
    # stopwords.union(["software", "software engineering"]) stopwords=stopwords
    wordcloud = WordCloud(max_font_size=50, background_color="white", max_words=100).generate_from_frequencies(
        freq_dict)
    # collocation_threshold = 3

    # Display the word cloud
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    # plt.show()


# Count number of rows/tuples
def count_rows():
    df = pd.read_csv(file_path, encoding='utf-8')
    values = ['Editorial']
    df = df[df['Document Type'].isin(values) == False]
    df.drop_duplicates(subset=['Titles'], inplace=True)
    print('Number of records', df.shape[0])


# TF-IDF for words in each year bracket.
def generate_tf_idf():
    data_frame = pd.read_csv(file_path_04)

    # Define a list of strings representing the author keywords
    keyword_array = []
    for value in data_frame['Author Keywords']:
        if str(value) != 'nan':
            keyword = str(value).lower().split('; ')
            keyword_array.extend(keyword)

    keyword_array = list(filter(lambda x: x != 'software engineering', keyword_array))
    # print(keyword_array)

    # Create a TfidfVectorizer object
    vectorized = TfidfVectorizer(ngram_range=(1, 2))

    # Compute the TF-IDF matrix
    tfidf_matrix = vectorized.fit_transform(keyword_array)

    # Extract the feature names (i.e., the keywords)
    feature_names = vectorized.get_feature_names_out()
    # print(feature_names)

    # Convert the TF-IDF matrix to a dictionary of keyword weights
    keyword_weights = dict(zip(feature_names, tfidf_matrix.toarray()[0]))
    # print(keyword_weights)

    df = pd.DataFrame(list(keyword_weights.items()), columns=["Word", "Weight"])
    df = df.sort_values(by='Weight', ascending=False)
    # print(df)
    # df.to_csv("/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 04/tf_idf_1.csv", mode='w+')

    # Generate the word cloud
    wordcloud = WordCloud(background_color='white').generate_from_frequencies(keyword_weights)

    # Display the word cloud
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.show()
