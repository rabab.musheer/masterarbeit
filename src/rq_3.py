import pandas as pd
import matplotlib.pyplot as plt


# Splitting combined_scopus_female data as per values in column 'Document Type'
def split_csv_per_document_type(doc_type):
    # Arrange authors who have contributed to 'Conference paper', 'Article', 'Review', 'Editorial' and 'Erratum' in different CSVs

    # Read CSV
    data_frame = pd.read_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/combined_genderized_female.csv')
    #

    # Get data from CSV with given doc_type
    values = [doc_type]
    data_frame = data_frame[data_frame['Document Type'].isin(values) == True]
    #

    # Reformat 'Author full names' into presentable form
    author_name = data_frame['Author full names'].str.split('(').str.get(0)
    author_name = author_name.str.split(',').str.get(-1) + author_name.str.split(',').str.get(0)
    data_frame['author_name'] = author_name
    #

    # Save CSV
    data_frame.to_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 03/cp_combined.csv' % doc_type,
                      mode='w+')


# Create dictionary with publications as key and the respective authors as values.
def combine_title_author():
    # Read CSV
    data_frame = pd.read_csv('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/highly_cited_papers.csv')

    # Dictionary with titles as keys and number of authors as values
    title_author_dict = {}

    for row in range(len(data_frame)):
        # if data_frame.loc[row, "Document Type"] != 'Erratum':
        # if data_frame.loc[row, "Cited by"] > 0:
        title = data_frame.loc[row, "Title"]
        if data_frame.loc[row, "Title"] not in title_author_dict:
            title_author_dict[title] = []
        title_author_dict[title].append(data_frame.loc[row, "Gender"])

    return title_author_dict


# Check for same element in array
def all_same(arr):
    return all(element == arr[0] for element in arr)


# Pie chart of distribution of male and female single authors
def single_author_pie_chart():
    title_author_dict = combine_title_author()

    # Titles having single authors
    single_author_arrays = [value for value in title_author_dict.values() if len(value) == 1 and (value[0] == 'male' or value[0] == 'female')]
    print(single_author_arrays)
    print(len(single_author_arrays))

    # num_of_docs = [key for key, value in title_author_dict.items() if
    #                isinstance(value, list) and len(value) == 1 and (value[0] == 'male' or value[0] == 'female')]
    # print(num_of_docs)
    # print("Number of documents", len(num_of_docs))

    # # Papers with only one author
    male_values = [sub_array[0] for sub_array in single_author_arrays if sub_array[0] == 'male']
    print(len(male_values))
    female_values = [sub_array[0] for sub_array in single_author_arrays if sub_array[0] == 'female']
    print(len(female_values))

    # create a pie chart
    labels = ['Male authors', 'Female authors']
    data = [len(male_values), len(female_values)]
    plt.pie(data, labels=labels, autopct='%1.0f%%', colors=['#007575', '#00D1D1'])
    # plt.title('Distribution of single author documents by gender')
    plt.show()


# Pie chart of distribution of male and female co-authors
def co_author_pie_chart():
    title_author_dict = combine_title_author()
    # print(title_author_dict)

    # Titles having two or more than two authors
    multiple_author_arrays = [value for value in title_author_dict.values() if len(value) >= 2 and
                              (set(value) == {'male', 'female'} or
                               set(value) == {'female', 'female'} or
                               set(value) == {'male', 'male'})]

    female_with_female = 0
    female_with_male = 0
    male_with_male = 0

    for sub_array in multiple_author_arrays:
        if 'female' in sub_array:
            if all_same(sub_array):
                female_with_female += 1
            else:
                female_with_male += 1
        elif 'male' in sub_array:
            if all_same(sub_array):
                male_with_male += 1
            # else:
            #     female_with_male += 1

    print('Total number of documents', len(multiple_author_arrays))
    print('Number of times female occurs with female:', female_with_female)
    print('Number of times female occurs with male:', female_with_male)
    print('Number of times male occurs with male:', male_with_male)

    # Create a pie chart
    labels = ['All male authors', 'All female authors', 'Both male and female authors']
    data = [male_with_male, female_with_female, female_with_male]
    plt.pie(data, labels=labels, autopct='%1.0f%%', colors=['#007575', '#00D1D1', '#5e9494'])
    # plt.title('Distribution of values by type')
    plt.show()


"""
Unused functions
"""
# def plot_citations_against_years(fp):
#     # Data frame from csv file
#     data_frame = pd.read_csv(fp, dtype={"Year": "string", "Cited by": "string"})
#     data_frame.groupby(data_frame['Year']).count()
#     data_frame.groupby(data_frame['Year']).count()['IncidntNum'].plot(kind='bar')
#     plt.show()

# # Plot graph
# fig, ax = plt.subplots()
# ax.plot(data_frame['Year'], data_frame['Cited by'])
# ax[1].set_xlabel('Cited by')
# ax[1].set_ylabel('Year')
#
# fig.tight_layout()
# # pd.crosstab(data_frame['Year'], data_frame['Cited by']).plot.bar(color=['#00D1D1'], width=0.8)  # '#007575'
# plt.show()

# plot_citations_against_years('/Users/ummerabab-/Desktop/Uni Passau/Thesis/csv_files/RQ 03/cp_combined_original.csv')
